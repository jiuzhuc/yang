/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const offsiteRouter = {
  path: '/offsite',
  component: Layout,
  redirect: 'noRedirect',
  name: 'offsite',
  meta: {
    title: 'offsite',
    icon: 'chart'
  },
  children: [
    {
      path: 'ba',
      component: () => import('@/views/offsite/ba'),
      name: 'ba',
      meta: { title: 'ba' }
    },
    {
      path: 'bb',
      component: () => import('@/views/offsite/bb'),
      name: 'bb',
      meta: { title: 'bb' }
    },
    {
      path: 'bd',
      component: () => import('@/views/offsite/bd'),
      name: 'bd',
      meta: { title: 'bd' }
    },
    {
      path: 'be',
      component: () => import('@/views/offsite/be'),
      name: 'be',
      meta: { title: 'be' }
    },
    {
      path: 'bf',
      component: () => import('@/views/offsite/bf'),
      name: 'bf',
      meta: { title: 'bf' }
    }
  ]
}

const statRouter = {
  path: '/stat',
  component: Layout,
  redirect: 'noRedirect',
  name: 'stat',
  meta: {
    title: '统计',
    icon: 'list'
  },
  children: [
    {
      path: 'stat',
      component: () => import('@/views/stat/index'),
      name: 'stat',
      meta: { title: 'stat' }
    }
  ]
}

export {
  offsiteRouter,
  statRouter
}
