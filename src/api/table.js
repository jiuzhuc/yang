import request from '@/utils/request'

export function getClueList(params) {
  return request({
    url: '/api/admin/clueinfo',
    method: 'get',
    params
  })
}

export function getClueStat(params) {
  return request({
    url: '/api/admin/clueDownload',
    method: 'get',
    params
  })
}

export function getAccessStat(params) {
  return request({
    url: '/api/admin/accessDownload',
    method: 'get',
    params
  })
}
