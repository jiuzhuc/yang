import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/auth/Login',
    // url: '/login/userLogin',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: `/api/admin/admininfo?token=${token}`,
    method: 'get'
    // data
  })
}

// export function login(data) {
//   return request({
//     url: '/login/userLogin',
//     method: 'post',
//     data
//   })
// }

// export function getInfo(token) {
//   return request({
//     url: '/user/info',
//     method: 'get',
//     params: { token }
//   })
// }

export function logout(data) {
  return request({
    url: '/api/auth/Logout',
    method: 'post',
    data
  })
}

export function logout2(data) {
  return request({
    url: '/api/auth/Logoutasdas',
    method: 'get',
    data
  })
}
