import axios from 'axios'
import { Message, Loading } from 'element-ui'
import { getToken } from '@/utils/auth'

const apiUrl = process.env.VUE_APP_BASE_API

export function getBlob(method, exportApi, dataFrom, fileName) {
  const loading = Loading.service({
    fullscreen: true,
    text: '拼命加载中....',
    background: 'rgba(0, 0, 0, 0.5)'
  })
  axios({
    method: method,
    url: apiUrl + exportApi,
    responseType: 'blob',
    data: dataFrom,
    headers: {
      Authorization: 'Bearer ' + getToken()
    }
  }).then(res => {
    console.log(res)
    if (res) {
      if (
        res.headers['content-type'] != null &&
        res.headers['content-type'].includes('json')
      ) {
        loading.close()
        // 此处拿到的data才是blob
        const { data } = res
        const reader = new FileReader()
        reader.onload = () => {
          const { result } = reader
          const errorInfos = JSON.parse(result)
          const { message } = errorInfos
          Message({
            message: message,
            type: 'error'
          })
        }
        reader.readAsText(data)
      } else {
        //eslint -disable-next-line
        // 以服务的方式调用的 Loading 需要异步关闭
        loading.close()

        var filename = fileName
        if (!filename) {
          filename = res.headers['filename']
          if (filename) {
            filename = decodeURI(filename)
          } else {
            filename = 'result'
          }
        }

        const content = res.data
        const blob = new Blob([content])
        const reader = new FileReader()
        reader.readAsDataURL(blob)
        reader.onload = (e) => {
          const a = document.createElement('a')
          a.download = filename
          a.href = e.target.result
          document.body.appendChild(a)
          a.click()
          document.body.removeChild(a)
        }

        // let url = window.URL.createObjectURL(res.data);
        // let link = document.createElement("a");
        // link.style.display = "none";
        // link.href = url;
        // link.setAttribute("download", filename + ".xls");
        // document.body.appendChild(link);
        // link.click();
      }
    }
  })
}
