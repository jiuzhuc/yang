import request from '@/utils/request'

// 线索
export function clueinfo(params) {
  return request({
    url: '/api/admin/clueinfo',
    method: 'get',
    params
  })
}

// 访问
export function accessinfo(params) {
  return request({
    url: '/api/admin/accessinfo',
    method: 'get',
    params
  })
}

// 统计数据
export function stat(params) {
  return request({
    url: '/api/admin/statistics',
    method: 'get',
    params
  })
}
